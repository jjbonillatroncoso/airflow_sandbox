
from kubernetes.client import V1VolumeMount
from kubernetes.client import V1Container, V1EnvVar, V1EmptyDirVolumeSource, V1Volume
from airflow import DAG
from airflow.providers.cncf.kubernetes.operators.kubernetes_pod import KubernetesPodOperator
from datetime import datetime, timedelta
import os
from airflow.models import Variable
from airflow.providers.amazon.aws.operators.eks import EKSPodOperator
import json



dbt_data = json.loads(Variable.get("dbt_connection"))
dbt_env_vars = [
    V1EnvVar(name="DBT_SNOWFLAKE_ACCOUNT",value=dbt_data["DBT_SNOWFLAKE_ACCOUNT"]),
    V1EnvVar(name="DBT_SNOWFLAKE_USER",value=dbt_data["DBT_SNOWFLAKE_USER"]),
    V1EnvVar(name="DBT_SNOWFLAKE_PASS",value=dbt_data["DBT_SNOWFLAKE_PASS"]),
    V1EnvVar(name="DBT_SNOWFLAKE_DB",value=dbt_data["DBT_SNOWFLAKE_DB"]),
    V1EnvVar(name="DBT_SNOWFLAKE_WH",value=dbt_data["DBT_SNOWFLAKE_WH"]),
    V1EnvVar(name="DBT_SNOWFLAKE_SCHEMA",value=dbt_data["DBT_SNOWFLAKE_SCHEMA"]),
    V1EnvVar(name="DBT_PROFILES_DIR",value="/src/data_dbt_demo")
]

dbt_image_init = V1Container(
    image="public.ecr.aws/e2e1t5k9/git-sync:v3.0.1",
    name="git-sync",
    env=[
        V1EnvVar(name="GIT_SYNC_REPO",value="https://gitlab.com/jjbonillatroncoso/data_dbt_demo.git"),
        V1EnvVar(name="GIT_SYNC_DEST",value="data_dbt_demo"),
        V1EnvVar(name="GIT_SYNC_ONE_TIME",value="true"),
        V1EnvVar(name="GIT_SYNC_BRANCH",value="main")
    ],
    volume_mounts=[V1VolumeMount(mount_path="tmp/git",name="markdown")]
    )

default_args = {
    "owner": "airflow",
    "depends_on_past": False,
    "email": ["support@airflow.com"],
    "email_on_failure": False,
    "email_on_retry": False,
    "retries": 1,
    "retry_delay": timedelta(minutes=5)
}


with DAG(
    dag_id='example_eks_operator',
    schedule_interval=None,
    start_date=datetime(2021, 1, 1),
    tags=['example'],
    default_args=default_args
) as dag:

    podRun = EKSPodOperator(
                cmds=["bash"],
                #arguments=["-c", """n=1 ; while true ; do echo -n "Command launch #$n: " ; sleep 1 ; n=$(( n + 1 )) ; done"""],
                arguments=["-c", "cd /src/data_dbt_demo/dbt_demo && dbt deps && dbt run"],
                labels={"task": "pod_run","pod_name":"podRun2"},
                #name="dbt-run",
                task_id="podRun",
                get_logs=True,
                dag=dag,
                in_cluster=False if os.environ.get("LOCAL") else True,
                namespace="airflow-staging",
                is_delete_operator_pod=False,
                init_containers=[dbt_image_init],
                image="public.ecr.aws/e2e1t5k9/data_dbt:amd64_1.2",
                volume_mounts=[V1VolumeMount(mount_path="/src",name="markdown")],
                volumes=[V1Volume(name="markdown",empty_dir=V1EmptyDirVolumeSource())],
                startup_timeout_seconds=520,
                env_vars=dbt_env_vars,
                cluster_name="jj-wizeline"       
            )


    